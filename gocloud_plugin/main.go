package main

import (
    "gocloud_plugin/terraform-provider-gocloud/gocloud"

    "github.com/hashicorp/terraform-plugin-sdk/plugin"
)

func main() {
    plugin.Serve(&plugin.ServeOpts{
        ProviderFunc: gocloud.Provider})
}
