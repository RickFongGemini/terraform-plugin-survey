package gocloud

import (
    "bytes"
    "encoding/json"
    "fmt"
    "log"

    "github.com/hashicorp/terraform-plugin-sdk/helper/schema"
)

type SolutionPostBody struct {
    Name		string	`json:"name"`
    Desc		string	`json:"desc,omitempty"`
    Category		string `json:"category,omitempty"`
    IsTenantAdminOnly	bool	`json:"is_tenant_admin_only,omitempty"`
}

type SolutionPatchBody struct {
    Name		string	`json:"name,omitempty"`
    Desc		string	`json:"desc,omitempty"`
    Category		string	`json:"category,omitempty"`
    IsPublic		bool	`json:"is_public,omitempty"`
    IsTenantAdminOnly	bool	`json:"is_tenant_admin_only,omitempty"`
}

func resourceSolution() *schema.Resource {
    return &schema.Resource{
        Create: resourceSolutionCreate,
        Read:   resourceSolutionRead,
        Update: resourceSolutionUpdate,
        Delete: resourceSolutionDelete,
        Importer: &schema.ResourceImporter{
            State: schema.ImportStatePassthrough,
        },

        Schema: map[string]*schema.Schema{
            "category": {
                Type:		schema.TypeString,
                Optional:	true,
            },

            "create_time": {
                Type:		schema.TypeString,
                Optional:	true,
                Computed:	true,
            },

            "desc": {
                Type:		schema.TypeString,
                Optional:	true,
            },

            "gsp_name": {           
                Type:		schema.TypeString,
                Optional:	true,
                Computed:	true,
            },

            "solution_id": {
                Type:		schema.TypeInt,
                Optional:       true,
                Computed:       true,
                ForceNew:	true,
            },

            "is_public": {
                Type:		schema.TypeBool,
                Optional:	true,
            },

            "is_tenant_admin_only": {
                Type:		schema.TypeBool,
                Optional:	true,
            },

            "name": {
                Type:		schema.TypeString,
                Required:	true,
            },

            "state": {
                Type:		schema.TypeString,
                Optional:	true,
                Computed:	true,
            },

            "version": {
                Type:		schema.TypeString,
                Optional:	true,
                Computed:	true,
            },
        },
    }
}

func resourceSolutionCreate(d *schema.ResourceData, meta interface{}) error {
    config := meta.(*PConfig)
    resourcePath := "solutions/"
    name := d.Get("name").(string)
    body := SolutionPostBody {
        Name:			name,
        Desc:			d.Get("desc").(string),
        Category:		d.Get("category").(string),
        IsTenantAdminOnly:	d.Get("is_tenant_admin_only").(bool),
    }

    buf := new(bytes.Buffer)
    json.NewEncoder(buf).Encode(body)
    response, err := config.doNormalRequest(resourcePath, "POST", buf)

    if err != nil {
        return fmt.Errorf("Error creating gocloud_solution %s: %s", name, err)
    }

    var data map[string]interface{}
    err = json.Unmarshal([]byte(response), &data)

    if err != nil {
        return err
    }

    d.SetId(fmt.Sprintf("%v", data["id"]))
    d.Set("solution_id", data["id"])
    return resourceSolutionRead(d, meta)
}

func resourceSolutionRead(d *schema.ResourceData, meta interface{}) error {
    config := meta.(*PConfig)
    solutionID := d.Id()
    resourcePath := fmt.Sprintf("solutions/%s/", solutionID)
    response, err := config.doNormalRequest(resourcePath, "GET", nil)

    if err != nil {
        return fmt.Errorf("Unable to retrieve solution %s: %s", solutionID, err)
    }

    var data map[string]interface{}
    err = json.Unmarshal([]byte(response), &data)

    if err != nil {
        return err
    }

    log.Printf("[DEBUG] Retrieved gocloud_solution %s", d.Id())

    d.Set("category", data["category"])
    d.Set("create_time", data["create_time"])
    d.Set("desc", data["desc"])
    d.Set("gsp_name", data["gsp_name"])
    d.Set("solution_id", data["id"])
    d.Set("is_public", data["is_public"])
    d.Set("is_tenant_admin_only", data["is_tenant_admin_only"])
    d.Set("name", data["name"])
    d.Set("state", data["state"])
    d.Set("version", data["version"])

    return nil
}

func resourceSolutionUpdate(d *schema.ResourceData, meta interface{}) error {
    config := meta.(*PConfig)
    _, newName := d.GetChange("name")
    _, newDesc := d.GetChange("desc")
    _, newCG := d.GetChange("category")
    _, newIP := d.GetChange("is_public")
    _, newITAO := d.GetChange("is_tenant_admin_only")
    body := SolutionPatchBody {
        Name:			fmt.Sprintf("%v", newName),
        Desc:			fmt.Sprintf("%v", newDesc),
        Category:		fmt.Sprintf("%v", newCG),
        IsPublic:		newIP.(bool),
        IsTenantAdminOnly:	newITAO.(bool),
    }

    solutionID := d.Id()
    resourcePath := fmt.Sprintf("solutions/%s/", solutionID)
    buf := new(bytes.Buffer)
    json.NewEncoder(buf).Encode(body)
    _, err := config.doNormalRequest(resourcePath, "PATCH", buf)

    if err != nil {
        return fmt.Errorf("Unable to update solution %s: %s", solutionID, err)
    }

    return resourceSolutionRead(d, meta)
}

func resourceSolutionDelete(d *schema.ResourceData, meta interface{}) error {
    config := meta.(*PConfig)
    solutionID := d.Id()
    resourcePath := fmt.Sprintf("solutions/%s/", solutionID)
    _, err := config.doNormalRequest(resourcePath, "DELETE", nil)

    if err != nil {
        return fmt.Errorf("Unable to delete solution %s: %s", solutionID, err)
    }

    d.SetId("")

    return nil
}
