package gocloud

import (
    "fmt"
    "encoding/json"

    "github.com/hashicorp/terraform-plugin-sdk/helper/resource"
)

func projectStateRefreshFunc(config *PConfig, resourcePath string, projectID int) resource.StateRefreshFunc {
    return func() (interface{}, string, error) {
        newPath := fmt.Sprintf("%s%d/", resourcePath, projectID)
        response, err := config.doNormalRequest(newPath, "GET", nil)
        if err != nil {
            //error handling
            return nil, "", err
        }

        var data map[string]interface{}
        err = json.Unmarshal([]byte(response), &data)
        
        if err != nil {
            //error handling
            return nil, "", err
        }

        return data, data["status"].(string), nil
    }
}
