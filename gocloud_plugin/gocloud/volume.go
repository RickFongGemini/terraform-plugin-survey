package gocloud

import (
    "fmt"
    "encoding/json"

    "github.com/hashicorp/terraform-plugin-sdk/helper/resource"
)

func flattenVolumeProjectInfo(v map[string]interface{}) []map[string]interface{} {
    projectInfo := make([]map[string]interface{}, 1)
    projectInfo[0] = map[string]interface{}{
        "id":	int(v["id"].(float64)),
        "name":	v["name"].(string),
    }
    return projectInfo
}

func volumeStateRefreshFunc(config *PConfig, resourcePath string, volumeID int) resource.StateRefreshFunc {
    return func() (interface{}, string, error) {
        newPath := fmt.Sprintf("%s%d/", resourcePath, volumeID)
        response, err := config.doNormalRequest(newPath, "GET", nil)
        if err != nil {
            //error handling
            return nil, "", err
        }

        var data map[string]interface{}
        err = json.Unmarshal([]byte(response), &data)
        
        if err != nil {
            //error handling
            return nil, "", err
        }

        return data, data["status"].(string), nil
    }
}
