package gocloud

import (
    "fmt"
    "log"
    "encoding/json"

    "github.com/hashicorp/terraform-plugin-sdk/helper/schema"
)

func dataSourceProject() * schema.Resource {
    return &schema.Resource{
        Read: dataSourceProjectRead,

        Schema: map[string]*schema.Schema{
            "name": {
                Type:		schema.TypeString,
                Optional:	true,
                ForceNew:	true,
            },

            "project_id": {
                Type:		schema.TypeInt,
                Required:	true,
            },

            "platform": {
                Type:           schema.TypeString,
                Required:       true,
            },
            "status": {
                Type:		schema.TypeString,
                Optional:	true,
                ForceNew:	true,
            },
            "status_reason": {
                Type:           schema.TypeString,
                Optional:       true,
                ForceNew:       true,
            },
            "uuid": {
                Type:           schema.TypeString,
                Optional:       true,
                ForceNew:       true,
            },
        },
    }
}

// dataSourceSolutionRead performs the solution lookup.
func dataSourceProjectRead(d *schema.ResourceData, meta interface{}) error {
    config := meta.(*PConfig)
    projectID := d.Get("project_id").(int)

    if projectID == 0 {
        return fmt.Errorf("Should specify 'solution_id' to retrieve GOC solution.")
    }
    platform := d.Get("platform").(string)
    resourcePath := fmt.Sprintf("%s/projects/%d/", platform, projectID)
    response, err := config.doNormalRequest(resourcePath, "GET", nil)

    if err != nil {
        return fmt.Errorf("Unable to retrieve solution %s: %s", projectID, err)
    }

    var data map[string]interface{}
    err = json.Unmarshal([]byte(response), &data)

    if err != nil {
        return err
    }   

    return dataSourceProjectAttributes(d, data)
}

// dataSourceProjectAttributes populates the fields of a solution resource.
func dataSourceProjectAttributes(d *schema.ResourceData, data map[string]interface{}) error {
    project_id := data["id"]
    log.Printf("[DEBUG] Retrieved gocloud_solution: %v", project_id)

    d.SetId(fmt.Sprintf("%v", data["id"]))
    d.Set("project_id", project_id)
    d.Set("name", data["name"])
    d.Set("status", data["status"])
    d.Set("status_reason", data["status_reason"])
    d.Set("uuid", data["uuid"])

    return nil
}
