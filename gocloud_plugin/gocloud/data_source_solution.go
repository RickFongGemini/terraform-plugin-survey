package gocloud

import (
    "fmt"
    "log"
    "encoding/json"

    "github.com/hashicorp/terraform-plugin-sdk/helper/schema"
)

func dataSourceSolution() * schema.Resource {
    return &schema.Resource{
        Read: dataSourceSolutionRead,

        Schema: map[string]*schema.Schema{
            "category": {
                Type:		schema.TypeString,
                Optional:	true,
                ForceNew:	true,
            },

            "create_time": {
                Type:		schema.TypeString,
                Optional:	true,
                ForceNew:	true,
            },

            "desc": {
                Type:		schema.TypeString,
                Optional:	true,
                ForceNew:	true,
            },

            "gsp_name": {           
                Type:		schema.TypeString,
                Optional:	true,
                ForceNew:	true,
            },

            "solution_id": {
                Type:		schema.TypeInt,
                Required:	true,
            },

            "is_public": {
                Type:		schema.TypeBool,
                Optional:	true,
                ForceNew:	true,
            },

            "is_tenant_admin_only": {
                Type:		schema.TypeBool,
                Optional:	true,
                ForceNew:	true,
            },

            "name": {
                Type:		schema.TypeString,
                Optional:	true,
                ForceNew:	true,
            },

            "state": {
                Type:		schema.TypeString,
                Optional:	true,
                ForceNew:	true,
            },

            "version": {
                Type:		schema.TypeString,
                Optional:	true,
                ForceNew:	true,
            },
        },
    }
}

// dataSourceSolutionRead performs the solution lookup.
func dataSourceSolutionRead(d *schema.ResourceData, meta interface{}) error {
    config := meta.(*PConfig)
    solutionID := d.Get("solution_id").(int)

    if solutionID == 0 {
        return fmt.Errorf("Should specify 'solution_id' to retrieve GOC solution.")
    }

    resourcePath := fmt.Sprintf("solutions/%d/", solutionID)
    response, err := config.doNormalRequest(resourcePath, "GET", nil)

    if err != nil {
        return fmt.Errorf("Unable to retrieve solution %s: %s", solutionID, err)
    }

    var data map[string]interface{}
    err = json.Unmarshal([]byte(response), &data)

    if err != nil {
        return err
    }   

    return dataSourceSolutionAttributes(d, data)
}

// dataSourceSolutionAttributes populates the fields of a solution resource.
func dataSourceSolutionAttributes(d *schema.ResourceData, data map[string]interface{}) error {
    solution_id := data["id"]
    log.Printf("[DEBUG] Retrieved gocloud_solution: %v", solution_id)

    d.SetId(fmt.Sprintf("%v", data["id"]))
    d.Set("category", data["category"])
    d.Set("create_time", data["create_time"])
    d.Set("desc", data["desc"])
    d.Set("gsp_name", data["gsp_name"])
    d.Set("solution_id", solution_id)
    d.Set("is_public", data["is_public"])
    d.Set("is_tenant_admin_only", data["is_tenant_admin_only"])
    d.Set("name", data["name"])
    d.Set("state", data["state"])
    d.Set("version", data["version"])

    return nil
}
