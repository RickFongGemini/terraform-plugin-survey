module gocloud_plugin/terraform-provider-gocloud

go 1.14

require github.com/hashicorp/terraform-plugin-sdk v1.15.0
